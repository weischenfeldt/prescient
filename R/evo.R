test_events <- function(x, events, rndm = NULL, col_rndm = "X1.04",
    n = 1000, prop = 1 / 4, cl) {
    col_rndm <- which(colnames(rndm) == col_rndm)
    sub_x <- tab_subsets(x, events)
    sub_test <- n_test(sub_x, n, prop = prop, cl = cl)
    # test_events <- names(sub_test)
    # sub_test <- sub_test
    max_x <- which.max(sub_test)

    res <- list()
    res[["test"]] <- sort(sub_test)
    res[["size"]] <- nrow(sub_x)
    res[["max"]] <- sub_test[max_x]
    if (is.null(rndm)) {
        # Nothing to do
    } else {
        res[["pred"]] <- pred_lookup(rownames(sub_x), rndm, col_rndm)
    }
    res
}

quick_evo <- function(x, events, rndm, col_rndm, split_char = ";",
                      n = 1000, exclude = NULL) {
    col_rndm <- which(colnames(rndm) == col_rndm)
    events <- do.call(c, strsplit(x = events, split = split_char))
    if (!is.null(exclude)) {
        exclude <- do.call(c, strsplit(x = exclude, split = split_char))
    }

    evidence <- get_evidence(x, events, exclude_events = exclude)
    res <- list()
    res[["size"]] <- length(evidence)
    res[["pred"]] <- pred_lookup(evidence, rndm, col_rndm)
    res
}

pred_lookup <- function(row_names, rndm, col_idx) {
    rndm_sub <- rndm[rownames(rndm) %in% row_names, ]
    mean(rndm_sub[, col_idx], na.rm = TRUE)
}

build_evo <- function(x, events, rndm, privacy = NULL,
                      col_rndm = "X1.04", split_char = ";",
                      n = 1000) {
    size <- c()
    pred <- c()
    dist <- c()
    for (i in 1:length(events)){
        ev_i <- events[1:i]
        ev_no_i <- events[!events %in% ev_i]
        if ( i == 1){
         distance <- 0
        } else if (is.null(privacy)) {
            distance <- 1
        } else {
            x1 <- events[i]
            x2 <- events[i - 1]
            distance <- get_pair_privacy(privacy, x1, x2)
        }
        test_i <- quick_evo(x, events[1:i], rndm, col_rndm,
                            split_char, n, ev_no_i)
        size <- c(size, test_i$size)
        pred <- c(pred, test_i$pred)
        dist <- c(dist, distance)
    }
    #dist <- dist / sum(dist, na.rm = TRUE)
    data.frame(event = events, size = size, pred = pred, dist = dist)
}


grow_events <- function(x, events, select = 2, n = 1000,
    prop = 1 / 4, cl = 2) {
    possible_next <- ncol(x) - length(events)
    x_sub <- tab_subsets(x, events)
    if (possible_next > 1 & nrow(x_sub) > 5) {
        message("re-compute the serie:")
        message("number of patients: ", nrow(x_sub))
        message("number of events: ", ncol(x_sub))

        res <- test_events(x, events, n = n, prop = prop, cl = cl)
        next_select <- names(tail(res$test, n = select))
        next_select <- sapply(next_select, FUN = function(x, y) c(y, x),
                              y = events, simplify = FALSE, USE.NAMES = FALSE)
        list(events = events,
             next_events = lapply(next_select, FUN =
                function(x, y, s, n, prop, cl) {
                grow_events(x = y, events = x, select = s, n = n,
                    prop = prop, cl = cl)
             },
             y = x, s = select, n = n, prop = prop, cl = cl))
    } else {
        list(events = events, next_events = NULL)
    }
}

unlist_branches <- function(evonodes, min_length = 1) {
    all_events <- as.character(unlist(evonodes, use.names = FALSE))
    init <- all_events[1]
    l_ev <- length(all_events)
    breaks <- cut(1:length(all_events), c(which(all_events == init), l_ev),
                  include.lowest = TRUE, right = FALSE)
    split_events <- split(x = 1:l_ev, f = breaks)
    all_branches <- lapply(split_events, FUN = function(x, e, l) {
            if (length(x) >= l) {
                e[x]
            }
        },
        e = all_events, l = min_length)
    all_branches[!sapply(all_branches, is.null)]
}

merge_events <- function(x, events, collapse = ";", overlaps = 0.95) {
    res <- c()
    progress_events <- c()
    events_group <- c()
   for (event in events) {
       if (!event %in% progress_events) {
           progress_events <- c(progress_events, event)
           exclude_events <- events[!events %in% progress_events]
           events_group <- c(events_group, event)
           sub_x <- tab_subsets(x, progress_events, present = TRUE)
           if (length(exclude_events) == 0) {
               coocc_x <- 0
           } else if (length(exclude_events) > 1) {
               coocc_x <- colSums(sub_x[, exclude_events]) / nrow(sub_x)
           } else {
               coocc_x <- sum(sub_x[, exclude_events]) / nrow(sub_x)
           }
           if (sum(coocc_x >= overlaps) > 0) {
               ov_events <- names(coocc_x)[which(coocc_x >= overlaps)]
               events_group <- c(events_group, ov_events)
               progress_events <- c(progress_events, ov_events)
           } else {
               res <- c(res, paste(sort(events_group), collapse = collapse))
               events_group <- c()
           }
       }
   }
   if (length(events_group) > 0) {
       res <- c(res, paste(sort(events_group), collapse = collapse))
   }
   res
}
