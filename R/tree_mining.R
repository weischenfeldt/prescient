prob_events <- function(x, events) {
    nth_events <- table(do.call("c", lapply(x, get_nth_elemnt,
        length(events) + 1)))
    nth_events / sum(nth_events)
}

get_nth_elemnt <- function(x, n) {
    if (length(x) >= n) {
        x[n]
    } else {
        NA
    }
}

compute_prob_branch <- function(branch, evo) {
    progress <- sapply(1:length(branch),
        FUN = function(x, y) y[1:x], y = branch)
    prob_branch <- lapply(progress, prob_events, x = evo)
    probs <- sapply(2:length(branch), FUN = function(x, prob_branch, branch) {
            prob_branch[[x - 1]][branch[x]]
        },
    prob_branch = prob_branch, branch = branch)
    probs
}


compute_prob_branch2 <- function(branch, events_mat, rndm, col_rndm) {
    progress <- sapply(1:length(branch),
        FUN = function(x, y) y[1:x], y = branch)
    prob_branch <- lapply(progress, FUN = function(x, events_mat,
        rndm, col_rndm) {
            pboptions(type = "none")
            res <- test_events(events_mat, x, rndm = rndm,
                col_rndm = col_rndm, n = 1000, 1)
            res$test
        },
        events_mat = events_mat, rndm = rndm, col_rndm = col_rndm)
    probs <- sapply(2:length(branch), FUN = function(x, prob_branch, branch) {
            prob_branch[[x - 1]][branch[x]]
        },
    prob_branch = prob_branch, branch = branch)
    probs
}

compute_stats_x <- function(branch, evo, event, events_mat, rndm, col_rndm) {
    if (event %in% branch) {
        probs <- compute_prob_branch(branch, evo)
        #probs <- compute_prob_branch2(branch, events_mat, rndm, col_rndm)
        idx_event <- which(names(probs) == event)
        prob <- prod(probs[1:idx_event])
        evidence <- get_evidence(events_mat, names(probs[1:idx_event]))
        pred <- pred_lookup(evidence, rndm, col_rndm)
    } else {
        idx_event <- 0
        prob <- 0
        pred <- NA
    }
    data.frame(event = event, prob = prob, idx = idx_event, pred = pred)
}

get_prob <- function(evo, events, events_mat, rndm,
    col_rndm = "X1.04", cl = 4) {
    # subset the evo object to the branch with the events in the query
    evo_sub <- function(x, y) {
        len_y <- length(y)
        if (sum(y %in% x[1:len_y]) == len_y) {
            x
        }
    }
    z <- lapply(evo, evo_sub, y = events)
    z[sapply(z, is.null)] <- NULL
    event_left <- unique(do.call("c", z))
    event_left <- event_left[!event_left %in% events]
    get_prob_x <- function(evo, event) {
        do.call(rbind, pblapply(evo, FUN = function(x, evo, event,
            events_mat, rndm, col_rndm)
            compute_stats_x(x, evo, event, events_mat, rndm, col_rndm),
            evo = evo, event = event, events_mat = events_mat,
            rndm = rndm, col_rndm = col_rndm, cl = cl))
    }
    stats <- lapply(event_left, get_prob_x, evo = z)
    stats <- do.call(rbind, stats)
    #probs <- unique(stats[, c("event", "prob", "idx")])
    #probs <- lapply(split(x = probs$prob, f = probs$event), sum)
    #data.frame(event = names(probs), probs = do.call("c", probs))
    stats
}


get_pfs_events <- function(events, events_mat, rndm, col_rndm = "X1.04", rm = 0) {
    rm_events <- function(freqs, events, n) {
        if ( n >= length(events)) {
            n <- length(events) - 1
        }
        freqs <- freqs[names(freqs) %in% events]
        freqs <- sort(freqs)
        len <- length(freqs)
        start <- 1 + n
        names(freqs[start:len])
    }
    freqs <- get_freq_events(events_mat)
    events_x <- rm_events(freqs, events, rm)
    evidence <- get_evidence(events_mat, events_x)
    pred <- pred_lookup(evidence, rndm, col_rndm)
    data.frame(events = paste(events_x, collapse=";"),n = length(evidence), pred = pred)
}

get_pfs_evo <- function(branch, events_mat, rndm, col_rndm = "X1.04", split_char = ";") {
    branch <- merge_events(branch = branch, split_char = split_char)
    res <- list()
    for (node in branch) {
        res_i <- get_pfs_events(node, events_mat, rndm, col_rndm)
        rm_i <- 0
        while(res_i$n < 3) {
            rm_i <- rm_i + 1
            message(rm_i, res_i$n)
            res_i <- get_pfs_events(node, events_mat, rndm, col_rndm, rm = rm_i)
        }
        res[[length(res) + 1]] <- res_i
    }
    do.call(rbind, res)
}


merge_events <- function(branch, split_char) {
    branch <- strsplit(x = branch, split = split_char)
    node_i <- c()
    res <- list()
    for (node in branch) {
        node_i <- c(node_i, node)
        res[[length(res) + 1]] <- node_i
    }
    res
}
