### Install

From bitbucket
```
library(devtools)
install_bitbucket("weischenfeldt/prescient@master",
                  auth_user = "ffavero", password = "***")
```

From local directory

```
library(devtools)
install_local("path/to/prescient")
```

### Run example

```
library(prescient)
library(igraph)
library(squash)

data(deletions_set)
deletions_set <- sample_to_rowname(deletions_set, "PID")

## Compute Branches (very slow with many branches):

perms <- pbapply::pblapply(1:10000, FUN = function(x, y) {
              shuffle_samples(y, prop = 1 / 4)
      },
      y = deletions_set)
branches <- pbapply::pblapply(perms, best_branch, cl = 4)

# or load the data:
# data(branches_10k)

# Compute edges from direct connecteion (l = 1)
ev_names <- colnames(deletions_set)
edges <- get_edges(branches, ev_names, l = 1)

net_rgas <- matrix(edges$w, ncol = length(ev_names),
     dimnames = list(ev_names, ev_names))
net_n_rgas <- matrix(edges$n, ncol = length(ev_names),
     dimnames = list(ev_names, ev_names))

# Find reasonable cutoff for node size:

plot(density(edges$n))

big_edges <- edges[edges$n >= 0.04, ]

# Create igraph object
g <- graph.data.frame(big_edges, directed=TRUE)
V(g)$label <- V(g)$name
E(g)$weight <- big_edges$w
E(g)$size <- big_edges$n

# count connections from/to

from_to <- lapply(ev_names, FUN = function(x, z) data.frame(from = sum(z$from == x), to = sum(z$to == x)), z= big_edges)
names(from_to) <- ev_names
from_to <- do.call(rbind, from_to)

# Write graphml file
write.graph(g, file = "EOPCA_direct_graph.graphml", format="graphml")

# Write matrices
write.table(net_rgas, "connections_weights_RGAs_EOPCA.txt", col.names = T, row.names = T, sep = "\t", quote = F)
write.table(net_n_rgas, "connections_N_RGAs_EOPCA.txt", col.names = T, row.names = T, sep = "\t", quote = F)

# plot stuff
pdf("network_RGAs_EOPCA.pdf", 18, 12)
    colorgram(net_n_rgas, xlabels = TRUE, ylabels = TRUE,las = 1, zlab = 'size', main = "number interactions")
    colorgram(net_rgas, xlabels = TRUE, ylabels = TRUE,las = 1, zlab = 'relative weights', main = "weights networks")
    plot(g)
    plot(density(edges$n), las = 1, main = "Size distribution nodes")
    abline(v = 0.04, lty = 3)
    barplot(t(from_to), beside = T, col = c("green", "red"), las = 1, main = "Connection from/to events")
    legend("topright", c("from","to"), bty = "n", fill = c("green", "red"))
dev.off()

```
