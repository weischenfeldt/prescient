### Install

From bitbucket
```
library(devtools)
install_bitbucket("weischenfeldt/prescient@master")

```

From local directory

```
# from the shell prompt
git clone https://ffavero@bitbucket.org/weischenfeldt/prescient.git

# in R

library(devtools)
install_local("prescient")

```

### Run example

Prepare the data:

 - A patient list with the status of some recurrent genomic alterations (RGAs)
 - A patient list with the random forest trained data to associate patients and survival
 - A patient list with the status of recurrently alterated pathways (=Based on the RGAs list)

```
library(prescient)

data(deletions_set)
data(random_set)
data(pathway_set)

deletions_set <- sample_to_rowname(deletions_set, "PID")
random_set <- sample_to_rowname(random_set, "PID")
pathway_set <- sample_to_rowname(pathway_set, "PID")

```

## Probablity of event free survival (PEFS) prediction

We use the information in the RGAs and in the random forest trained lists to associate RGAs with predicted outcome. For example let's start to predict the outcome of a patient presenting with an alteration in ERG only:

```
get_pfs_evo("ERG", deletions_set, random_set)

# PEFS result

 events n      pred
 ERG  118 0.8628625

```

The RGAs list may also contain marker such as the Purity-adjusted Epigenetic Prostate Cancer Index (PEPCI). Let's hipotyze the patient has ERG and is detected as PEPCI high:

```
get_pfs_evo(c("ERG;PEPCI_H"), deletions_set, random_set)

 events     n      pred
PEPCI_H;ERG 32 0.7675801

```

## Prediction of the most likely next event

PRESCIENT build a conditional probabilistic model to predict the most likely next event, based on the set of RGAs.

![PRESCIENT](fig/PRESCIENT_readme1.png)

With a set of RGA we need randomize a set of possible branches:

```
perms <- pbapply::pblapply(1:10000, FUN = function(x, y) {
              shuffle_samples(y, prop = 1 / 4)
      },
      y = deletions_set)
branches <- pbapply::pblapply(perms, best_branch, cl = 4)

# or load the pre-computed branches_10k data

data(branches_10k)
branch_name <- lapply(branches, names)

```

```
get_prob(branch_name, "ERG", deletions_set, random_set)


```

## Pathway prediction

Using a matrix of pathways rather then RGAs, it is possible to apply PRESCIENT to predict the pathway most likely effected in the next evolution step

```
perms <- pbapply::pblapply(1:10000, FUN = function(x, y) {
              shuffle_samples(y, prop = 1 / 4)
      },
      y = pathway_set)
branches <- pbapply::pblapply(perms, best_branch, cl = 4)

branch_name <- lapply(branches, names)

get_prob(branch_name_n, "ETS_fusion",pathway_set, random_set)


```
![pathways](fig/PRESCIENT_readme2.png)
