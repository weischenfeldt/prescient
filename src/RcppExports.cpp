#include <RcppArmadillo.h>

using namespace Rcpp;

// curveball
arma::mat curveball (arma::mat m);
RcppExport SEXP _prescient_curveball(SEXP mSEXP){
BEGIN_RCPP
    RObject rcpp_result_gen;
    RNGScope rcpp_rngScope_gen;
    traits::input_parameter< arma::mat >::type m(mSEXP);
    rcpp_result_gen = wrap(curveball(m));
    return rcpp_result_gen;
END_RCPP
}

// exclusivity
arma::mat pairwise_privacy_score(arma::mat deletions);
RcppExport SEXP _prescient_pairwise_privacy_score(SEXP deletionsSEXP){
BEGIN_RCPP
    RObject rcpp_result_gen;
    RNGScope rcpp_rngScope_gen;
    traits::input_parameter< arma::mat >::type deletions(deletionsSEXP);
    rcpp_result_gen = wrap(pairwise_privacy_score(deletions));
    return rcpp_result_gen;
END_RCPP
}

static const R_CallMethodDef CallEntries[] = {
    {"_prescient_curveball", (DL_FUNC) &_prescient_curveball, 1},
    {"_prescient_pairwise_privacy_score", (DL_FUNC) &_prescient_pairwise_privacy_score, 1},
    {NULL, NULL, 0}
};

RcppExport void R_init_prescient(DllInfo *dll) {
    R_registerRoutines(dll, NULL, CallEntries, NULL, NULL);
    R_useDynamicSymbols(dll, FALSE);
};
