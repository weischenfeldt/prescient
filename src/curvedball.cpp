#include <RcppArmadillo.h>

using namespace Rcpp;

// [[Rcpp::depends(RcppArmadillo)]]

arma::uvec setdiff(arma::uvec& x, arma::uvec& y){
    arma::uvec a = arma::unique(x);
    arma::uvec b = arma::unique(y);
    for (int j = 0; j < b.n_elem; j++) {
        arma::uvec q1 = arma::find(a == b(j));
        if (!q1.empty()) {
            a.shed_row(q1(0));
        }
    }
    return a;
}

arma::uvec sample_int(arma::uvec& pool, int n) {
    IntegerVector pool2 = IntegerVector(pool.begin(), pool.end()); 
    IntegerVector ret = Rcpp::sample(pool2, n, false);
    return as<arma::uvec>(ret);
}


arma::mat curveball(const arma::mat m){
    int R = m.n_rows;
    int C = m.n_cols;
    arma::mat rm = arma::mat(R, C, arma::fill::zeros);
	Rcpp::List hp(R);
    arma::uvec i_vec;
	for (int row = 0; row < R; row++) {
        i_vec = arma::find(m.row(row) == 1);
        hp[row] = i_vec;
    }
    arma::uvec pool = arma::linspace<arma::uvec>(0, R - 1, R);
	for (int rep = 0; rep < (5*R); rep++){
		arma::uvec AB = sample_int(pool, 2);
		arma::uvec a = hp[AB(0)];
		arma::uvec b = hp[AB(1)];
		arma::uvec ab = arma::intersect(a, b);
		int l_ab = ab.n_elem;
		int l_a = a.n_elem;
		int l_b = b.n_elem;
        if (l_ab != l_a && l_ab != l_b) {
            arma::uvec cab = join_cols(a, b);
			arma::uvec tot = setdiff(cab, ab);
			int l_tot = tot.n_elem;
			arma::uvec tot2 = sample_int(tot, l_tot);
			int L = l_a - l_ab;
            arma::uvec tot21 = tot2(arma::linspace<arma::uvec>(0, L - 1, L));
            arma::uvec tot22 = tot2(arma::linspace<arma::uvec>(L,  l_tot - 1, l_tot - L));
			hp[AB(0)] = join_cols(ab, tot21);
			hp[AB(1)] = join_cols(ab, tot22);
        }
	}
    for (int row = 0;  row < R; row ++){
        arma::uvec row_vec(1);
        row_vec(0) = row;
        arma::uvec row_idx = hp[row];
        rm(row_vec, row_idx).fill(1);
    }
	return(rm);
}
