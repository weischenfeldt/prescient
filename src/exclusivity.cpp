#include <RcppArmadillo.h>

using namespace Rcpp;

// [[Rcpp::depends(RcppArmadillo)]]


double exclusivity_ij(arma::mat& dels, int&  i, int& j) {
    int r = dels.n_rows;
    double e = 0;
    int n = 0;
    for (int k = 0; k < r; k++) {
        if(dels.row(k)(i) + dels.row(k)(j) == 2) {
            e += (1 / (arma::sum(dels.row(k))));
            n++;
        }
    }
    return (e / n);
}

arma::mat pairwise_privacy_score(arma::mat deletions) {
    int c = deletions.n_cols;
    arma::mat m(c, c, arma::fill::zeros);
    for (int i = 0; i < c; i++) {
        for (int j = 0; j < c; j++) {
            if (i <= j) {
                m(i, j) = exclusivity_ij(deletions, i, j);
            }
        }
    }
    return arma::symmatu(m);
}

/* R implementation
get_indexes <- function(row, dels) {
    if (!is.na(dels[row, 1]))
        which(dels[row, ] == 1)
}

pairwise_privacy_score2 <- function(dels, words) {

    privacy_mat <- matrix(rep(0, ncol(dels) ^ 2),
                          ncol = ncol(dels),
                          nrow = ncol(dels))
    for (i in 1:ncol(dels)) {
        for (j in i:ncol(dels)) {
            if (i < j)
                privacy_mat[i, j] <-
                    mean(1 / unlist(lapply(x_words, FUN = function(x) {
                        if (all(c(i, j) %in% x)) {
                            length(x)
                        }
                    })))
            else if (i == j) {
                privacy_mat[i, j] <-
                    mean(1 / unlist(lapply(x_words, FUN = function(x) {
                        if (i %in% x) {
                            length(x)
                        }
                    })))
           }
       }
   }
   colnames(privacy_mat) <- rownames(privacy_mat) <- colnames(dels)
   # convert to symmetric
   privacy_mat[lower.tri(privacy_mat)] <- t(privacy_mat)[lower.tri(privacy_mat)]
   privacy_mat
}

privacy_mat2 <- function(x) {
    x_words <- sapply(1:nrow(x), get_indexes, x)
    pairwise_privacy_score2(x, x_words)
}
*/